(
    () => {
        angular.module("Snake", []).controller(
            "SnakeVM", [
                '$timeout',
                '$window',
                SnakeVM
            ]
        );

        SnakeVM.$inject = ['$timeout', '$window'];

        function SnakeVM($timeout,$window) {
            const game = this;
            const BOARD_SIZE = 20;
            const DIRECTIONS = {
                LEFT: 65,
                UP: 87,
                RIGHT: 68,
                DOWN: 83
            };
            const COLOURS = {
                GAME_OVER: "#820303",
                FRUIT: "#E80505",
                SNAKE_HEAD: "#078F00",
                SNAKE_BODY: "#0DFF00",
                BOARD: "#000"
            };
            let snake = {
                direction: DIRECTIONS.LEFT,
                parts: [
                    { x: -1, y: -1 }
                ]
            };
            let fruit = {
                x: -1,
                y: -1
            };
            let interval, tempDirection, isGameOver;

            game.score = 0;

            game.setStyling = (col, row) => {
                if (isGameOver) {
                    return COLOURS.GAME_OVER;
                } else if (fruit.x == row && fruit.y == col) {
                    return COLOURS.FRUIT;
                } else if (snake.parts[0].x == row && snake.parts[0].y == col) {
                    return COLOURS.SNAKE_HEAD;
                } else if (game.board[col][row] === true) {
                    return COLOURS.SNAKE_BODY;
                };
                return COLOURS.BOARD;
            };
            
            const update = () => {
                let newHead = getNewHead();
                console.log(getNewHead());
                console.log("boardCollision(newHead): ", boardCollision(newHead));
                console.log("selfCollision(newHead): ", selfCollision(newHead));

                if (boardCollision(newHead) || selfCollision(newHead)) {
                    return gameOver();
                } else if (fruitCollision(newHead)) {
                    eatFruit();
                };

                // Remove tail
                let oldTail = snake.parts.pop();
                game.board[oldTail.y][oldTail.x] = false;

                // Pop tail to head
                snake.parts.unshift(newHead);
                game.board[newHead.y][newHead.x] = true;

                // Do it again
                snake.direction = tempDirection;
                $timeout(update, interval);
            };

            const getNewHead = () => {
                let newHead = angular.copy(snake.parts[0]);

                // Update Location
                if (tempDirection === DIRECTIONS.LEFT) {
                    newHead.x -= 1;
                } else if (tempDirection === DIRECTIONS.RIGHT) {
                    newHead.x += 1;
                } else if (tempDirection === DIRECTIONS.UP) {
                    newHead.y -= 1;
                } else if (tempDirection === DIRECTIONS.DOWN) {
                    newHead.y += 1;
                };
                return newHead;
            };

            const boardCollision = (part) => {
                return part.x === BOARD_SIZE || part.x === -1 || part.y === BOARD_SIZE || part.y === -1;
            };

            const selfCollision = (part) => {
                console.log("part = ", part);
                console.log("[part.y] = ", part.y);
                console.log("game.board[part.y] = ", game.board[part.y]);
                console.log("[part.x] = ", part.x);
                console.log("game.board[part.y][part.x] = ", game.board[part.y][part.x]);
                return game.board[part.y][part.x] === true;
            };

            const fruitCollision = (part) => {
                return part.x === fruit.x && part.y === fruit.y;
            };

            const resetFruit = () => {
                let x = Math.floor(Math.random() * BOARD_SIZE);
                let y = Math.floor(Math.random() * BOARD_SIZE);

                if (game.board[y][x] === true) {
                    return resetFruit();
                };
                fruit = { x: x, y: y };
            };

            const eatFruit = () => {
                game.score++;
                // Grows by 1
                let tail = angular.copy(snake.parts[snake.parts.length - 1]);
                snake.parts.push(tail);
                resetFruit();

                if (game.score % 5 === 0) {
                    interval -= 15;
                }
            };

            const gameOver = () => {
                isGameOver = true;

                $timeout(
                    () => {
                        isGameOver = false;
                    }
                , 500);

                setupBoard();
            };

            const setupBoard = () => {
                game.board = [];
                for (let i = 0; i < BOARD_SIZE; i++) {
                    game.board[i] = [];
                    for (let j = 0; j < BOARD_SIZE; j++) {
                        game.board[i][j] = false;
                    }
                };
            };

            setupBoard();

            $window.addEventListener(
                "keyup",
                (e) => {
                    console.log("Keyup: ", e.keyCode);
                    if (e.keyCode == DIRECTIONS.LEFT && snake.direction !== DIRECTIONS.RIGHT) {
                        tempDirection = DIRECTIONS.LEFT;
                    } else if (e.keyCode == DIRECTIONS.UP && snake.direction !== DIRECTIONS.DOWN) {
                        tempDirection = DIRECTIONS.UP;
                    } else if (e.keyCode == DIRECTIONS.RIGHT && snake.direction !== DIRECTIONS.LEFT) {
                        tempDirection = DIRECTIONS.RIGHT;
                    } else if (e.keyCode == DIRECTIONS.DOWN && snake.direction !== DIRECTIONS.UP) {
                        tempDirection = DIRECTIONS.DOWN;
                    };
                }
            );
            game.start = () => {
                game.score = 0;
                snake = {
                    direction: DIRECTIONS.LEFT,
                    parts: []
                };
                tempDirection = DIRECTIONS.LEFT;
                isGameOver = false;
                interval = 150;

                // Set up initial snake
                for (let i = 0; i < 5; i++) {
                    snake.parts.push({
                        x: 10 + i,
                        y: 10
                    });
                };
                resetFruit();
                update();
            };
        };
    }
)();